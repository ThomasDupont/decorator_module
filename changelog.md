##V0.1.6

- Correct mismatch in Readme

##V0.1.5

- Add params binding in annoted methods
- Support read decorator from top to bottom
- add a catch on parse exception

##V0.1.4

- Update and clean documentation
- Remove unused dependencies

##V0.1.3

- Add a response listener to get the response and do something with it
- Add a required marker on Body/QueryParam decorator

##V0.1.2

- Bug correction on email management in Controller
- Update README to be more clear

##V0.1.1

- Add QueryParam and BodyParam decorator
- Add param parse for in call decorator