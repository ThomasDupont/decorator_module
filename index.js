/**
 *
 *
 * @package
 * @licence MIT
 * @author Thomas Dupont
 */

if (Number(process.version.match(/^v(\d+\.\d+)/)[1]) < 8) {
    throw new Error('Decorator package: your node version must be minimum 8');
}

module.exports = {
    launch: dir => require('./lib/app/DecoratorFactory').launch(dir),
    router: require('./lib/app/Router'),
    addDecorators: dir => require('./lib/app/DecoratorListener').initDecoratorList(dir),
    response: require('./lib/app/Response'),
    responseListener: require('./lib/app/ResponseListener')
};
