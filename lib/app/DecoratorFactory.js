const fs = require('fs');
const decorator = require('./DecoratorListener');
const log = require('./../log/log');

const controllers = {};

function readRecursive(dir) {
	if (!fs.statSync(dir).isDirectory()) {
		throw new Error(`${dir}is not a directory`);
	}
	if (dir.indexOf('node_module') === -1) {
		const files = fs.readdirSync(dir);
		files.forEach((file) => {
			if (fs.statSync(dir + file).isDirectory()) {
				readRecursive(`${dir + file}/`);
			} else if (file.indexOf('Controller') !== -1) {
				const fileString = file.split('.').shift();
				decorator.getDecorator(dir, fileString, '.js');
				/* eslint-disable */
				controllers[fileString] = require(dir + fileString);
                /* eslint-enable */
			}
		});
	}
}

class DecoratorFactory {
	/**
	 *
     * @param dir {string} The directory with the controller
     */
	static launch(dir) {
		log.info('start parse decorator', 'FgYellow');
		decorator.initDecoratorList(`${__dirname}/../decorator`);
		readRecursive(dir);
	}

	/**
	 *
     * @param controller {string}
     * @param method {string}
     * @param req {object}
	 * @param additionalParams {array<Object>}
     * @returns {Promise.<*>}
     */
	static async init(controller, method, req, additionalParams) {
		return controllers[`${controller}Controller`][`${method}Action`](req, ...additionalParams);
	}

	/**
	 *
     * @returns {Array<object>}
     */
	static getDecorator() {
		return decorator.decoratorActions;
	}
}

module.exports = DecoratorFactory;
