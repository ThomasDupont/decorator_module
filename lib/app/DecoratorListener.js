/**
 *
 *
 * @package
 * @licence MIT
 * @author Thomas Dupont
 */

const fs = require('fs');
const Response = require('./Response');
const ParseAnnotation = require('./ParseAnnotation');
const log = require('./../log/log');

class DecoratorListener {
	constructor() {
		this.decoratorActions = [];
		this.decoratorInstance = [];
		this.launch = false;
	}

	/**
     *
     * @param dir
     * @param file
     * @param ext
     */
	getDecorator(dir, file, ext) {
		const result = ParseAnnotation.parseSingleFile(dir, file, ext);
		if (result.methods.length !== 0) {
			result.methods
				.forEach(m => m.decorators
					.map(d => this.executeDecoratorInBuild(d, result.controller, m.method)));
			this.decoratorActions.push(result);
		}
		this.launch = true;
	}

	/**
     * @void
     */
	initDecoratorList(dir) {
		if (this.launch) {
			log.warning(`Launch still done, the analyse for ${dir} as not effect`, 'BgYellow');
		}

		const files = fs.readdirSync(dir);
		files.forEach((file) => {
			this.decoratorInstance[
				file.split('.')
					.shift()
					.match('(.*)(?=Decorator)')
					.shift()
			/*eslint-disable */
			] = require(`${dir}/${file}`);
            /* eslint-enable */
		});
	}

	/**
     *
     * @param build
     * @param controller
     * @param method
     */
	executeDecoratorInBuild(build, controller, method) {
		const instance = this.decoratorInstance[build.call];
		if (instance && instance.inBuild) {
			instance.execute(controller, method, ...build.params);
		}
	}

	/**
     *
     * @param req
     * @param decorator
     * @param controller
     * @param method
     * @returns {Response}
     */
	async executeDecoratorInCall(req, decorator, controller, method) {
		const decoratorInstance = this.decoratorInstance[decorator.call];

		if (!decoratorInstance.inBuild) {
			return decoratorInstance.execute(controller, method, req, ...decorator.params);
		}
		return new Response(true);
	}
}

module.exports = new DecoratorListener();
