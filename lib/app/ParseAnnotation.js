/**
 *
 *
 * @package decorator-module
 * @licence MIT
 * @author Thomas Dupont
 */

const fs = require('fs');

const CONTROLLER = new RegExp(/(.*)(?=Controller)/);
const METHOD = new RegExp(/[a-zA-Z]*(?=Action)/);
const ANNOTATION = new RegExp(/@(.*?\()/);
const ANNOTATION_NAME = new RegExp(/^[a-zA-Z]*/);
const ANNOTATION_PARAM = new RegExp(/'(.*?)'/gm);

class ParseAnnotation {
	/**
     *
     * @param dir
     * @param file
     * @param ext
     * @returns {{controller, methods: Array}}
     */
	static parseSingleFile(dir, file, ext) {
		const methods = [];
		try {
			fs.readFileSync(dir + file + ext)
				.toString()
				.split('\n')
				.filter(line => line.match(METHOD) || line.match(ANNOTATION))
				.reverse()
				.forEach((e) => {
					const method = (e.match(METHOD) || []).shift();
					if (method === '') return;
					if (method !== undefined) {
						methods.push({
							method,
							decorators: [],
						});
					} else {
						methods[methods.length - 1].decorators.push({
							call: e.split('@').pop().match(ANNOTATION_NAME).shift(),
							params: (e.match(ANNOTATION_PARAM) || []).map(brut => brut.replace(/'/gm, '')),
						});
					}
				});
		} catch (e) {
			throw new Error(`Parse annotation failed, please check the annotation syntax. Error ${e.message}`);
		}

		Object.keys(methods).map(key => methods[key].decorators.reverse());

		return {
			controller: file.match(CONTROLLER).shift(),
			methods,
		};
	}
}

module.exports = ParseAnnotation;
