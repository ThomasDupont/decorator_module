/**
 *
 *
 * @package decorator-module
 * @licence MIT
 * @author Thomas Dupont
 */

const DecoratorFactory = require('./DecoratorFactory');
const DecoratorListener = require('./DecoratorListener');
const ResponseListener = require('./ResponseListener');
const Response = require('./Response');

class Render {
	/**
     *
     * @param c Controller
     * @param m Method
     * @param req The request
     * @param res The reponse event
     */
	static async render(c, m, req, res) {
		const decorators = DecoratorFactory.getDecorator();

		const affectedController = decorators.find(e => e.controller === c);
		const affectedMethod = affectedController.methods.find(e => e.method === m)
			|| { decorators: [] };

		let additionalParams = [];
		for (let i = 0; i < affectedMethod.decorators.length; i += 1) {
			const rDecorator = await DecoratorListener.executeDecoratorInCall(
				req,
				affectedMethod.decorators[i],
				c,
				m,
			);

			if (!(rDecorator instanceof Response)) {
				throw new TypeError(`The return type of 
					${affectedMethod.decorators[i].call}Decorator must be a instance of Response. 
					${typeof rDecorator} given`);
			}

			if (!rDecorator.success) {
				return ResponseListener.sendDecoratorResponse(rDecorator, res);
			}

			if (rDecorator.result.additionalCallParams) {
				additionalParams = additionalParams.concat(rDecorator.result.additionalCallParams);
			}
		}
		return DecoratorFactory.init(c, m, req, additionalParams).then(resp => ResponseListener.sendControllerResponse(resp, res));
	}
}

module.exports = Render;
