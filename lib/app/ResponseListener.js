
class ResponseListener {
	/**
     *
     * @param response Response
     * @param res
     */
	static sendDecoratorResponse(response, res) {
		return res.status(response.code).send(response.result);
	}

	static onDecoratorResponse(callback) {
		this.sendDecoratorResponse = callback;
	}

	/**
     *
     * @param response Response
     * @param res
     */
	static sendControllerResponse(response, res) {
		return res.status(response.code).send(response.result);
	}

	static onControllerResponse(callback) {
		this.sendControllerResponse = callback;
	}
}

module.exports = ResponseListener;
