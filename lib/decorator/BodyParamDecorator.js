
const Response = require('./../app/Response');
const Param = require('./../util/Param');
/**
 * Decorator call each time
 *
 * @package decorator-module
 * @licence MIT
 * @author Thomas Dupont
 *
 * @type call Decorator
 * @Method execute(controller, method, request, ...rest): Response
 */
class BodyParam {
	static execute(controller, method, req, param, regex, required = 'true') {
		const check = req.body[param];

		try {
			Param.isRequired(required, check, param);
			Param.matchRegex(check, param, regex);
		} catch (e) {
			return new Response(false, 400, { error: e.message });
		}

		return new Response(true);
	}
}

BodyParam.inBuild = false;
module.exports = BodyParam;
