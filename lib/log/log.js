/**
 *
 *
 * @package
 * @licence MIT
 * @author Thomas Dupont
 */

const reference = require('./reference');

module.exports = {
	info: (msg, color = false, prefix = '[INFO] Decorator module: ') => {
		if (color === false) {
			return console.info(prefix + msg);
		}
		return console.info(`${reference.colors[color]}%s\x1b[0m`, prefix + msg);
	},
	warning: (msg, color = false, prefix = '[WARNING] Decorator module: ') => {
		if (color === false) {
			return console.warn(prefix + msg);
		}
		return console.warn(`${reference.colors[color]}%s\x1b[0m`, prefix + msg);
	},
};
