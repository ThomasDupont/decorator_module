/**
 *
 *
 * @package decorator-module
 * @licence MIT
 * @author Thomas Dupont
 */


class Param {
	static isRequired(required, check, param) {
		if (required === 'true' && check === undefined) {
			throw new Error(`param ${param} missing`);
		}
	}

	static matchRegex(check, param, regex) {
		if (check === undefined) {
			return;
		}
		if (!check.match(new RegExp(regex))) {
			throw new TypeError(`param ${param} doesn't match requirement`);
		}
	}
}

module.exports = Param;
