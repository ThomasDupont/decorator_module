
let assert = require('assert');
let { decorator } = require('./server/server');
let DecoratorListener =  require('./../lib/app/DecoratorListener');

describe('Parse file', function() {
    describe('Controller', function() {
        it('Check node version', function() {
            assert.equal(Number(process.version.match(/^v(\d+\.\d+)/)[1]) > 8, true);
        });
        it('launch analyse', function() {
            assert.equal(DecoratorListener.decoratorActions.length, 3);

            if (DecoratorListener.decoratorActions.length > 0) {
                DecoratorListener.decoratorActions.forEach(action => {
                    assert.equal(['es5', 'es6', 'not-annoted'].includes(action.controller), true);
                    action.methods.forEach(m => assert.notEqual(m.method, ''));
                });
                assert.equal(decorator.router.router.stack.length, 3);
            }
        });
    });
});
