/**
 *
 *
 * @package
 * @licence MIT
 * @author Thomas Dupont
 */

const Response = require('./../../lib/app/Response');

module.exports = {
    /**
     * @Route('/init', 'POST')
     * @BodyParam('id', '^([0-9a-f]*)$')
     *
     * @param req
     */
    initAction: function(req) { return new Response(true, 200, {action : 'init'}) }
};
