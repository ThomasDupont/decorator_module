const Response = require('./../../lib/app/Response');

/**
 * Main controller class
 */
class es6Controller {
	/**
	 * @Route('/info', 'POST')
	 * @BodyParam('id', '^[0-9a-f]*$')
	 *
     * @param req
     * @returns {Response}
     */
	static mainAction(req) {
        /**
		 *
         */
        var test = 'test@test.com';
		return new Response(true, 200, { message: 'api ok' });
	}

	/**
	 *
     * @param req
     * @returns {Response}
     */
	static testAction(req) {
		return new Response(true, 200, { message: 'test Action' });
	}

    /**
	 * @Route('/customer', 'GET')
	 * @QueryParam('id', '^[0-9a-f]*$')
     * @param req
     */
	static getInfoAction(req) {
        return new Response(true, 200, { result: {
        	lastname: 'Dupont',
			firstname: 'Thomas'
		}});
	}
}

module.exports = es6Controller;

