const Response = require('./../../lib/app/Response');

class MainController {

    static mainAction(req) {
        return new Response(true, 200, { message: 'api ok' });
    }

    static testAction(req) {
        return new Response(true, 200, { message: 'test action' });
    }

    static getInfoAction(req) {
        return new Response(true, 200, { result: {
            lastname: 'Dupont',
            firstname: 'Thomas'
        }});
    }
}

module.exports = MainController;

