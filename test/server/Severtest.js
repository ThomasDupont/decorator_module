let assert = require('assert');
const rp = require('request-promise');

describe('test call', function () {
    it ('test simple get with query param return error overrided', function(done) {
        rp.get('http://localhost:3000/customer?id=test').catch(e => {
            assert.equal(e.message, '400 - "{\\"error\\":true,\\"message\\":\\"param id doesn\'t match requirement\\"}"');
            done();
        });
    });
    it ('test simple get with query param return 200 overrided', function(done) {
        rp.get('http://localhost:3000/customer?id=06ef5', {json: true}).then(r => {
            assert.equal(r.result.lastname, 'Dupont');
            done();
        });
    });
    it ('test simple post with query param return 200 overrided', function(done) {
        rp.post('http://localhost:3000/info', {json: true, body: {
                id: '06ef5'
            }}).then(r => {
            assert.equal(r.message, 'api ok');
            done();
        });
    });
});