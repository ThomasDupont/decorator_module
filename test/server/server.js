const decorator = require('./../../index');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});
decorator.launch(__dirname + '/../../');
decorator.responseListener.onDecoratorResponse((response, res) => res.status(response.code).send({
    error: true,
    message : response.result.error
}));

decorator.responseListener.onControllerResponse((response, res) => res.status(response.code).send(response.result));

app.use('/', decorator.router.router);

app.listen(3000, () => {
    console.log('launched 3000');
});

module.exports = { app, decorator };